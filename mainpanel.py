from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QVBoxLayout,
    QTableWidget,
    QTableWidgetItem,
    QPushButton,
    QWidget,
    QLabel,
    QMessageBox,
)
from PyQt5.QtCore import pyqtSlot
from thermostat_logic import Thermostat


class ThermostatApp(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Thermostat App")
        self.setGeometry(100, 100, 800, 600)

        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)

        layout = QVBoxLayout()
        self.central_widget.setLayout(layout)

        self.table_widget = QTableWidget()
        self.table_widget.setColumnCount(2)
        layout.addWidget(self.table_widget)

        self.status_label = QLabel()
        layout.addWidget(self.status_label)

        self.lock_status_label = QLabel()
        layout.addWidget(self.lock_status_label)

        self.refresh_button = QPushButton("Refresh Schedule")
        self.refresh_button.clicked.connect(self.refresh_schedule)
        layout.addWidget(self.refresh_button)

        self.update_button = QPushButton("Update Schedule")
        self.update_button.clicked.connect(lambda: self.update_schedule_button(True))
        layout.addWidget(self.update_button)

        self.toggle_button = QPushButton("Enable/Disable Thermostat")
        self.toggle_button.setCheckable(True)
        self.toggle_button.clicked.connect(self.toggle_thermostat)
        layout.addWidget(self.toggle_button)

        self.thermostat = None
        self.schedule_data = None  

    @pyqtSlot(str, str)
    def open_with_device_info(self, serial_number, ip_address):
        """Initialize ThermostatApp with device information."""
        self.thermostat = Thermostat(serial_number, ip_address)
        self.load_schedule()
        self.update_lock_status()  
        self.show()

    def update_status_label(self):
        """Update the status label based on the current state of the thermostat."""
        if self.thermostat is not None:
            self.status_label.setText(
                "Thermostat is enabled"
                if self.thermostat.is_on()
                else "Thermostat is disabled"
            )
        else:
            self.status_label.setText("Thermostat not initialized")

    def load_schedule(self):
        """Load thermostat schedule and display in table."""
        day_names = {
            "0": "Monday",
            "1": "Tuesday",
            "2": "Wednesday",
            "3": "Thursday",
            "4": "Friday",
            "5": "Saturday",
            "6": "Sunday",
        }

        schedule = self.thermostat.get_schedule()
        if schedule:
            self.schedule_data = schedule  
            self.table_widget.setRowCount(len(self.schedule_data))
            for i, (day_index, periods) in enumerate(self.schedule_data.items()):
                day_name = day_names.get(day_index, "Unknown")
                self.table_widget.setItem(i, 0, QTableWidgetItem(day_name))
                schedule_text = " | ".join(
                    [
                        f"{time_minutes // 60}:{time_minutes % 60:02} - {int(temperature / 10)} °C"
                        for time_minutes, temperature in periods  
                    ]
                )
                self.table_widget.setItem(i, 1, QTableWidgetItem(schedule_text))
        else:
            self.table_widget.setRowCount(1)
            self.table_widget.setItem(
                0, 0, QTableWidgetItem("Failed to fetch schedule")
            )

        self.update_status_label()



    def refresh_schedule(self):
        """Refresh the thermostat schedule and update the table."""
        self.load_schedule()

    def update_schedule_button(self, send_requests=True):
        print("send_requests parameter:", send_requests)
        day_names = {
            "Monday": 0,
            "Tuesday": 1,
            "Wednesday": 2,
            "Thursday": 3,
            "Friday": 4,
            "Saturday": 5,
            "Sunday": 6,
        }

        modified_days = []
        for day_name, day_index in day_names.items():
            time_temp_str = self.table_widget.item(day_index, 1).text()
            time_temp_list = time_temp_str.split(" | ")
            periods = []
            for time_temp in time_temp_list:
                time_str, temp_str = time_temp.split(" - ")
                hour, minute = map(int, time_str.split(":"))
                temperature = int(float(temp_str.replace(" °C", "")) * 10)
                time_minutes = hour * 60 + minute
                periods.append([time_minutes, temperature])

            current_periods = self.thermostat.get_schedule().get(str(day_index), [])
            if current_periods != periods:
                modified_days.append((day_index, periods))
                print(f"Changes detected for {day_name}. Sending update for this day.")

        if modified_days:
            if send_requests:
                update_successful = self.thermostat.update_schedule(modified_days)
                if update_successful:
                    self.show_message_box("Success", "Thermostat schedule updated successfully.")
                else:
                    self.show_message_box("Error", "Failed to update thermostat schedule.")
                modified_days.clear()
            else:
                self.show_message_box("Warning", "Changes detected, but requests disabled.")
        else:
            self.show_message_box("Information", "No valid schedule data to update.")



    def show_message_box(self, title, message):
        msg_box = QMessageBox()
        msg_box.setWindowTitle(title)
        msg_box.setText(message)
        msg_box.exec_()
    
    def toggle_thermostat(self):
        """Disable or enable the thermostat."""
        if self.thermostat.lock_check() != "No lock":
            print("LAN control is locked. Cannot toggle thermostat.")
            return

        if self.toggle_button.isChecked():
            self.toggle_button.setText("Enable Thermostat")
            self.thermostat.turn_off() 
        else:
            self.toggle_button.setText("Disable Thermostat")
            self.thermostat.turn_on()  

        
        self.update_status_label()

    def update_lock_status(self):
        """Update the lock status label and disable buttons if necessary."""
        lock_status = self.thermostat.lock_check()
        self.lock_status_label.setText(f"Lock Status: {lock_status}")

        if lock_status == "LAN lock" or lock_status == "Both cloud and LAN lock":
            self.lock_status_label.setStyleSheet("color: red")
            self.update_button.setEnabled(False)
            self.toggle_button.setEnabled(False)
        else:
            self.lock_status_label.setStyleSheet("color: green")
            self.update_button.setEnabled(True)
            self.toggle_button.setEnabled(True)


if __name__ == "__main__":
    app = QApplication([])
    window = ThermostatApp()
    window.show()
    app.exec_()
