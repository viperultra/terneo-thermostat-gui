import sys
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QVBoxLayout,
    QPushButton,
    QLabel,
    QListWidget,
    QListWidgetItem,
    QWidget,
)
from PyQt5.QtCore import Qt, pyqtSignal, QObject, QTimer
from PyQt5.QtCore import QThread, pyqtSignal
import socket
import time
import json
import os
from mainpanel import ThermostatApp


class DeviceFoundSignal(QObject):
    device_found = pyqtSignal(dict, tuple)


def parse_json_data(data):
    try:
        parsed_data = json.loads(data)
        return parsed_data
    except json.JSONDecodeError:
        return None


class ScanningThread(QThread):
    device_found = pyqtSignal(dict)

    def __init__(self):
        super().__init__()
        self.sender_address = None
        self.stop_requested = False  

    def run(self):
        UDP_IP = "0.0.0.0"
        UDP_PORT = 23500

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind((UDP_IP, UDP_PORT))

        print("Listening for devices...")

        while not self.stop_requested: 
            data, addr = sock.recvfrom(1024)
            print("Received data:", data)
            print("Received from address:", addr)
            self.sender_address = addr
            parsed_data = parse_json_data(data.decode("utf-8"))
            if parsed_data:
                self.device_found.emit(parsed_data)

    def stop(self):  
        self.stop_requested = True


class DeviceDiscoveryApp(QMainWindow):
    device_selected = pyqtSignal(str, str)

    def __init__(self):
        super().__init__()
        self.scanning_thread = ScanningThread()
        self.scanning_thread.device_found.connect(
            lambda info: self.add_device_to_list(
                info, self.scanning_thread.sender_address
            )
        )
        self.setWindowTitle("Device Discovery")
        self.setGeometry(100, 100, 400, 300)

        layout = QVBoxLayout()

        self.discovery_label = QLabel("Searching for devices...")
        layout.addWidget(self.discovery_label)

        self.device_list_widget = QListWidget()
        layout.addWidget(self.device_list_widget)

        self.choose_button = QPushButton("Choose")
        self.choose_button.setEnabled(False)
        self.choose_button.clicked.connect(self.choose_device)
        layout.addWidget(self.choose_button)

        self.discover_button = QPushButton("Start Device Discovery")
        self.discover_button.clicked.connect(self.toggle_discovery)
        layout.addWidget(self.discover_button)

        central_widget = QWidget()
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)

        self.device_list_widget.itemClicked.connect(self.update_choose_button_state)
        self.unique_devices = set()  

    def toggle_discovery(self):
        if self.scanning_thread.isRunning():
            self.stop_discovery()
        else:
            self.start_discovery()

    def start_discovery(self):
        self.discovery_label.setText("Listening for devices...")
        self.discover_button.setText("Stop Device Discovery") 
        self.scanning_thread.start()

    def stop_discovery(self):
        self.discovery_label.setText("Discovery Stopped")
        self.discover_button.setText("Start Device Discovery")  
        if self.scanning_thread.isRunning():
            self.scanning_thread.stop() 

    def add_device_to_list(self, device_info, address):
        serial_number = device_info.get("sn")
        ip_address = address[0]
        device_identifier = (serial_number, ip_address)  
        if device_identifier not in self.unique_devices:  
            self.unique_devices.add(device_identifier)  
            device_info_str = f"SN: {serial_number}, IP: {ip_address}"
            item = QListWidgetItem(device_info_str)
            self.device_list_widget.addItem(item)

    def update_choose_button_state(self):
        self.choose_button.setEnabled(bool(self.device_list_widget.selectedItems()))

    def choose_device(self):
        selected_items = self.device_list_widget.selectedItems()
        if selected_items:
            selected_item = selected_items[0]
            selected_text = selected_item.text()
            sn_start = selected_text.find("SN: ") + 4
            sn_end = selected_text.find(",", sn_start)
            serial_number = selected_text[sn_start:sn_end]
            ip_start = selected_text.find("IP: ") + 4
            ip_address = selected_text[ip_start:]
            self.stop_discovery() 
            self.device_selected.emit(serial_number, ip_address)
            self.close()



if __name__ == "__main__":
    
    app = QApplication(sys.argv)
    search_window = DeviceDiscoveryApp()
    thermostat_window = ThermostatApp()

    search_window.device_selected.connect(thermostat_window.open_with_device_info)

    search_window.show()
    sys.exit(app.exec_())
